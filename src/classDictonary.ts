import {Activity} from "./datamodels/factdag/factDagResources/activity";
import {Authority} from "./datamodels/factdag/factDagResources/authority";
import {Process} from "./datamodels/factdag/factDagResources/process";
import {Fact} from "./datamodels/factdag/factDagResources/fact";
import {BinaryFact} from "./datamodels/factdag/factDagResources/binaryFact";
import {MessageBroker} from "./datamodels/factdag/factDagResources/messageBroker";
import {Directory} from "./datamodels/factdag/factDagResources/directory";
import {ActivityCandidate} from "./datamodels/factdag/candidates/activityCandidate";
import {AuthorityCandidate} from "./datamodels/factdag/candidates/authorityCandidate";
import {ProcessCandidate} from "./datamodels/factdag/candidates/processCandidate";
import {FactCandidate} from "./datamodels/factdag/candidates/factCandidate";
import {BinaryFactCandidate} from "./datamodels/factdag/candidates/binaryFactCandidate";
import {MessageBrokerCandidate} from "./datamodels/factdag/candidates/messageBrokerCandidate";
import {ResourceCandidate} from "./datamodels/factdag/candidates/resourceCandidate";

type EnumDictionary<T extends string | symbol | number, U> = {
    [K in T]: U;
};

export enum FACTDAG_CLASS {
    ACTIVITY = "Activity",
    AUTHORITY = "Authority",
    PROCESS = "Process",
    FACT = "Fact",
    BINARY = "BinaryFact",
    BROKER = "MessageBroker",
    DIRECTORY = "Directory",
}

export const classDictionary: EnumDictionary<FACTDAG_CLASS, any> = {
    [FACTDAG_CLASS.ACTIVITY]: Activity,
    [FACTDAG_CLASS.AUTHORITY]: Authority,
    [FACTDAG_CLASS.PROCESS]: Process,
    [FACTDAG_CLASS.FACT]: Fact,
    [FACTDAG_CLASS.BINARY]: BinaryFact,
    [FACTDAG_CLASS.BROKER]: MessageBroker,
    [FACTDAG_CLASS.DIRECTORY]: Directory,
};

export const candidateDictionary: EnumDictionary<FACTDAG_CLASS, any> = {
    [FACTDAG_CLASS.ACTIVITY]: ActivityCandidate,
    [FACTDAG_CLASS.AUTHORITY]: AuthorityCandidate,
    [FACTDAG_CLASS.PROCESS]: ProcessCandidate,
    [FACTDAG_CLASS.FACT]: FactCandidate,
    [FACTDAG_CLASS.BINARY]: BinaryFactCandidate,
    [FACTDAG_CLASS.BROKER]: MessageBrokerCandidate,
    [FACTDAG_CLASS.DIRECTORY]: ResourceCandidate,
};