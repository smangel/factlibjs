import {IUsesFromAuthorityString} from "../datamodels/interfaces/IUsesFromAuthorityString";
import {logger} from "@i5/factlib-utils";
import {SubscriptionManager} from "./subscriptionManager";

import {Client, IMessage} from "@stomp/stompjs";
import {FactClient} from "../factClient/FactClient";
Object.assign(global, { WebSocket: require("websocket").w3cwebsocket });

export class StompClient extends SubscriptionManager {
    constructor(service: FactClient<any>) {
        super(service);
    }

    /**
     *  Establishes connections to all MessageBrokers that are responsible for the resources defined in the
     *  configuration via STOMP and subscribes to the declared resources.
     */
    public async startSubscription(uses: IUsesFromAuthorityString[], callback: (resource: string) => void) {
        logger.debug("Attempting to start Subscription");
        for (const subscription of await this.getSubscriptions(uses)) {
            const connectOptions = {
                brokerURL: subscription.broker.getBrokerURL().toString(),
                debug: (str: string) => {
                    logger.verbose(str);
                },
                heartbeatIncoming: 4000,
                heartbeatOutgoing: 4000,
                reconnectDelay: 5000,
            };
            const client = new Client(connectOptions);
            const selectedResources = subscription.resources
                .map((element: URL) => "ActivityStreamObjectId = '" + element.toString() + "'");
            const subscribeHeaders = {
                ack: "client-individual",
                selector: selectedResources.join(" OR "),
            };
            const destination = subscription.broker.getOutputName();

            const receive = (message: IMessage) => {
                if (message.headers) {
                    message.ack();
                    logger.info("A subscribed resource changed: %s", message.headers.ActivityStreamObjectId);
                    callback(message.headers.ActivityStreamObjectId);
                } else {
                    logger.info("got empty message");
                }
            };

            client.onConnect = (frame) => {
                client.subscribe(destination, receive, subscribeHeaders);
            };

            client.onStompError = (frame) => {
                logger.error("Broker reported error: " + frame.headers.message);
                logger.error("Additional details: " + frame.body);
            };
            client.activate();
        }
    }
}