import {IUsesFromAuthorityString} from "../IUsesFromAuthorityString";

export interface ProcessConfig
{
    checkLatest: boolean;
    location: string;
    uses: IUsesFromAuthorityString[];
}
