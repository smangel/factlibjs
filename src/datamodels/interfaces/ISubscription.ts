import {MessageBroker} from "../factdag/factDagResources/messageBroker";

export interface ISubscription {
    broker: MessageBroker;
    resources: URL[];
}