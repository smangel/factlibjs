import {IResourceID} from "./IResourceID";
import {IFactID} from "./IFactID";

/**
 * The IDFactory has to be implemented by a concreate FactClient.
 * It defines, how concrete URIs are parsed to abstract ResourceIDs and FactIDs.
 */
// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface IDFactory {
    /**
     * Takes a URI and parses a FactID consisting of AuthorityID, ResourceID and RevisionID.
     * @param url The concrete URI representing a FactID.
     * @returns The Abstract FactID object
     */
    parseURIToFactID(url: string): IFactID;

    /**
     * Takes a URI and parses a ResourceID consisting of AuthorityID and ResourceID.
     * @param url The concrete URI representing a ResourceID.
     * @returns The Abstract ResourceID object
     */
    parseURIToResourceID(url: string): IResourceID;

    createResourceID(authorityID: string, resourceID: string): IResourceID;
    createFactID(authorityID: string, resourceID: string, revisionID: string): IFactID;

    createAuthorityResourceID(authorityID: string): IResourceID;


}