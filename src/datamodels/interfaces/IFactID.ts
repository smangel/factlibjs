import {IResourceID} from "./IResourceID";

export interface IFactID extends IResourceID {
    readonly revisionID: string;
    readonly factURI: string;
}