import {AuthorityCandidate} from "../factdag/candidates/authorityCandidate";
import {ActivityCandidate} from "../factdag/candidates/activityCandidate";
import {ProcessCandidate} from "../factdag/candidates/processCandidate";
import {FactCandidate} from "../factdag/candidates/factCandidate";
import {MessageBrokerCandidate} from "../factdag/candidates/messageBrokerCandidate";
import {IResourceID} from "./IResourceID";
import {Stream} from "stream";
import {BinaryFactCandidate} from "../factdag/candidates/binaryFactCandidate";

/**
 */
// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface FactDagResourceFactory {
    createEmptyAuthorityCandidate(resourceID: IResourceID): AuthorityCandidate;
    createEmptyActivityCandidate(resourceID: IResourceID): ActivityCandidate;
    createEmptyProcessCandidate(resourceID: IResourceID): ProcessCandidate;
    createEmptyFactCandidate(resourceID: IResourceID): FactCandidate;
    createEmptyBinaryFactCandidate(resourceID: IResourceID, binaryStream: Stream, contentType: string): BinaryFactCandidate;
    createEmptyBrokerCandidate(resourceID: IResourceID): MessageBrokerCandidate;
}