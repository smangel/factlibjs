export interface IResourceID {
    readonly authorityID: string;
    readonly resourceID: string;
    readonly directory: URL;
    readonly resourceURI: URL;
    readonly factURI: string;
    readonly resourceName: string;
    toString: () => string;
}