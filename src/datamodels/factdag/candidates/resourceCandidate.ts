import {Namespaces} from "@i5/factlib-utils";
import {Resource} from "../../resource";
import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a mutable ResourceCandidate that may become an immutable LDP resource at some point.
 */
export abstract class ResourceCandidate<TTrunk = EmptyTrunk> extends Resource<TTrunk> {

    protected constructor(data?: string, resourceID?: IResourceID, trunk?: TTrunk) {
        super(data, resourceID, trunk);
    }

    /**
     * Removes all PROV relation entries from the current resource.
     * Exposes protected clearProvenance() from [[Resource]]
     */
    public clearProv() {
        this.clearProvenance();
    }

    /**
     * Sets a Fact-Relation in the local store of the current resource.
     */
    protected addFactRelation(relation: string, object: string): void {
        const uri = this.resourceID.toString();
        const elem = this._store.sym(uri);
        this._store.add(elem,
            Namespaces.FACT(relation),
            object,
            elem.doc());
    }
}
