import {Fact} from "./fact";
import {Stream} from "stream";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a BinaryFact. <br />
 * BinaryFacts are Facts with an additional binary payload.
 */
export class BinaryFact<TTrunk = any> extends Fact {
    constructor(private _binaryContent: Stream | string, store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk, private _contentType?: string) {
        super(store, factID, authorityResourceID, trunk);
    }


    get binaryContent(): Stream | string {
        return this._binaryContent;
    }

    get contentType(): string | undefined{
        return this._contentType;
    }
}