import {Namespaces} from "@i5/factlib-utils";
import {Fact} from "./fact";
import {LdpFactID} from "../../../factClient/ldpMementoClient/mementoHandler/resources/ldpFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a MessageBroker Entity from the LDP.
 * Each Authority declares a MessageBroker that is responsible for distributing change messages.
 * A single broker can be responsible for multiple authorities.
 * It is expected to exist under a single authority that declares its details with a messageBroker fact.
 */
export class MessageBroker<TTrunk = any> extends Fact<TTrunk> {
    constructor(store: any, factID: LdpFactID, authorityResourceID: IResourceID, trunk: TTrunk) {
        super(store, factID, authorityResourceID, trunk);
    }

    /**
     * Retrieves the URL of the broker from the local store.
     * @return The address under which the broker is reachable.
     */
    public getBrokerURL(): URL {
        const rdfResult = this.queryStore(Namespaces.SC(Namespaces.SCHEMA_RELATION.URL));
        return new URL(rdfResult[0].object.value);
    }

    /**
     * Retrieves the name of the output topic/queue from the local store.
     * @return The name of the topic/queue that change notifications for LDP resources are published to.
     * Processes interested in updates from this broker should subscribe to this topic/queue to receive notifications.
     */
    public getOutputName(): string {
        const rdfResult = this.queryStore(Namespaces.FACT(Namespaces.FACT_RELATION.OUTPUTS_TO));
        return rdfResult[0].object.value;
    }

    /**
     * Retrieves the output type (topic/queue) from the local store.
     * @return The type (topic/queue) describing how change notifications for LDP resources are published.
     */
    public getOutputType(): string {
        const rdfResult = this.queryStore(Namespaces.FACT(Namespaces.FACT_RELATION.HAS_OUTPUT_TYPE));
        return rdfResult[0].object.value;
    }
}
