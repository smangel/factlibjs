import {Term} from "rdf-js";
import {logger, Namespaces} from "@i5/factlib-utils";
import {LdpResourceID} from "../factClient/ldpMementoClient/mementoHandler/resources/ldpResourceID";
import {IResourceID} from "./interfaces/IResourceID";
import {ContentType, TurtleContentType} from "rdflib/lib/types";

const rdf = require("rdflib");
const PROV = Namespaces.PROV;
const PROV_RELATION = Namespaces.PROV_RELATION;
/**
 * Representation of a Resource that can be read from or written to a Linked Data Platform.
 */
export class Resource<TTrunk> {
    protected _store: any;
    private _resourceID: IResourceID;
    private _trunk: TTrunk | undefined;

    /**
     */
    constructor(store?: any, resourceID?: IResourceID, trunk?: TTrunk) {
        if (trunk) {
            this._trunk = trunk;
        }
        if (resourceID) {
            this._resourceID = resourceID;
        } else {
            this._resourceID = new LdpResourceID("http://127.0.0.1/", "");
        }
        if (store) {
            if (typeof store === 'string' || store instanceof String) {
                this._store = rdf.graph();
                rdf.parse(store, this._store, this._resourceID.resourceURI.toString(), "text/turtle");
            } else {
                this._store = store;
            }
        } else {
            this._store = rdf.graph();
        }
    }

    get resourceID(): IResourceID {
        return this._resourceID;
    }

    get trunk(): TTrunk | undefined {
        return this._trunk;
    }

    set trunk(value: TTrunk | undefined) {
        this._trunk = value;
    }

    public get store(): any {
        return this._store;
    }

    public set store(value: any) {
        this._store = value;
    }

    /**
     * Serializes the content of the RDF store.
     * @param contentType Choose the desired serialization by passing a RDFlib ContentType. Turtle is the default serialization.
     * Other useful serialiazions could be RDFXMLContentType, JSONLDContentType or N3ContentType.
     * @returns A string containing a Turtle serialization of the store and
     * reformats it to be compatible with Trellis.
     */
    public serialize(contentType: ContentType = TurtleContentType): string {
        if (this._resourceID.resourceURI === undefined) {
            return "";
        } else {
            let serialization =  rdf.serialize(undefined, this._store, this._resourceID.resourceURI.origin + "/", contentType);
            if (contentType == TurtleContentType) {
                serialization =  serialization.replace("<>", "</>");
            }
            return serialization;
        }
    }

    /**
     * Queries the local RDF store.
     */
    public queryStore(predicate: Term | RegExp, object?: Term | RegExp, subject?: Term | RegExp): any {
        let res;
        if (subject) {
            res = this._store.sym(subject);
        } else {
            res = this._store.sym(this._resourceID.resourceURI.toString());
        }
        return this._store.match(res, predicate, object);
    }

    /**
     * Removes all PROV relation entries from the current resource.
     */
    protected clearProvenance(): void {
        const uri = this._resourceID.resourceURI.toString();
        logger.debug("Cleared PROV of %s", uri);
        const elem = this._store.sym(this._resourceID.resourceURI.toString());
        this._store.removeMany(elem, PROV(PROV_RELATION.WAS_ATTRIBUTED_TO), null, null, false);
        this._store.removeMany(elem, PROV(PROV_RELATION.WAS_GENERATED_BY), null, null, false);
        this._store.removeMany(elem, PROV(PROV_RELATION.WAS_REVISION_OF), null, null, false);
        this._store.removeMany(elem, PROV(PROV_RELATION.USED), null, null, false);
        this._store.removeMany(elem, PROV(PROV_RELATION.WAS_ASSOCIATED_WITH), null, null, false);
        this._store.removeMany(elem, PROV(PROV_RELATION.ACTED_ON_BEHALF_OF), null, null, false);
    }

    /**
     * Adds PROV type to current resource, declaring it for example as an Entity, an Activity or an Agent.
     * @param object The object string of the RDF triple, must be of the supported PROV-Types.
     */
    protected addProvenanceType(object: Namespaces.PROV_TYPE): void {
        const uri = this._resourceID.resourceURI.toString();
        logger.debug("Added PROV-Type: %s a %s", uri, object);
        const elem = this._store.sym(uri);
        this._store.add(elem, Namespaces.RDF(Namespaces.RDF_RELATION.TYPE), PROV(object), elem.doc());
    }

    /**
     * Adds PROV type to current resource, declaring it for example as an Entity, an Activity or an Agent.
     * @param relation The predicate string of the RDF triple. Must be a supported Prov-Relation.
     * @param object The object of the RDF triple. This should be the URI of an existing resource.
     */
    protected addProvenanceRelation(relation: Namespaces.PROV_RELATION, object: string): void {
        const uri = this._resourceID.resourceURI.toString();
        logger.debug("Added PROV-Relation: %s %s %s", uri, relation, object);
        const elem = this._store.sym(uri);
        this._store.add(elem, PROV(relation), object, elem.doc());
    }

    /**
     * Updates existing PROV relation. To avoid multiple entries where only one entry is plausible,
     * the old entry for that relation is deleted first
     * (e.g. only one activity referenced via wasGeneratedBy makes sense).
     * @param relation The intended PROV relation (RDF predicate).
     * @param object The URI of the resource that is the intended object of the PROV triple.
     */
    protected replaceProvenanceRelation(relation: Namespaces.PROV_RELATION, object?: string): void {
        const elem = this._store.sym(this._resourceID.resourceURI.toString());
        this._store.removeMany(elem, PROV(relation), null, null, false);
        if (object) {
            this.addProvenanceRelation(relation, object);
        }
    }
}
