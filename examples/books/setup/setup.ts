import { LdpMementoClient, MessageBroker, logger, Directory } from "../../../index";
import { LdpTrunk } from "../../../src/factClient/ldpMementoClient/resources/ldpTrunk";
import { IResourceID } from "../../../src/datamodels/interfaces/IResourceID";

const service = new LdpMementoClient();
const factDagResourceFactory = service.getFactDagResourceFactory();
const idFactory = service.getIDFactory();
const dep1 = "http://localhost:8081/";
const dep2 = "http://localhost:8082/";

async function createDirIfNotExists(resourceID: IResourceID): Promise<void> {
    return service.createDirectory(resourceID).catch(e => {
        if (!e.response || (e.response as XMLHttpRequest).status !== 409) throw e;
        // return service.getDirectory(resourceID);
    }).then( () => Promise.resolve() );
};

async function setupEnvironment(
    ldpServerURL: string,
    brokerResourceID: string,
    brokerOutputName: string,
    brokerOutputType: string,
    brokerURL: URL): Promise<void> {

    // Ensure FactDAG LDP directory structure is present is expected
    const wellKnown = idFactory.createResourceID(ldpServerURL, ".well-known");
    await createDirIfNotExists(wellKnown);

    const authorityID = idFactory.createAuthorityResourceID(ldpServerURL);
    await service.getLastAuthorityRevision(authorityID).catch(e => {
        // only acceptable reason for failure is 'not found' or Trellis-specific 'not acceptable'
        if (!e.response || (e.response.status !== 404 && e.response.status !== 406)) throw e;

        logger.info("Authority not yet configured. Trying to create!");
        const brokerID = idFactory.createResourceID(ldpServerURL, brokerResourceID);
        return (service.getLastResourceRevision(brokerID) as Promise<MessageBroker<LdpTrunk>>)
            .catch(e => {
                // only acceptable reason for failure is 'not found' or Trellis-specific 'not acceptable'
                if (!e.response || (e.response.status !== 404 && e.response.status !== 406)) throw e;

                const brokerCandidate = factDagResourceFactory.createEmptyBrokerCandidate(brokerID);
                //new MessageBrokerCandidate("", slug, brokerFactURI);
                brokerCandidate.addOutputName(brokerOutputName);
                brokerCandidate.addOutputType(brokerOutputType);
                brokerCandidate.addURL(brokerURL);
                return service.createMessageBrokerResource(brokerCandidate);
            }).then(broker => {
                const authCandidate = factDagResourceFactory.createEmptyAuthorityCandidate(authorityID);
                authCandidate.addResposibleBroker(broker);
                return service.createAuthorityResource(authCandidate);
            });
    }).then(authority => Promise.all([
        createDirIfNotExists(idFactory.createResourceID(authority.resourceID.authorityID, "facts")),
        createDirIfNotExists(idFactory.createResourceID(authority.resourceID.authorityID, "processes")),
        createDirIfNotExists(idFactory.createResourceID(authority.resourceID.authorityID, "activities")),
    ]));

}

Promise.all([
    setupEnvironment(dep1, "broker1", "output", "topic", new URL("tcp://localhost:61613")),
    setupEnvironment(dep2, "broker2", "output", "topic", new URL("tcp://localhost:61614")),
]).then(()=> logger.info("Setup completed successfully!"));
