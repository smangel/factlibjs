# Book Publishing Example
 This set of examples consists of the following elements: 
 * Setup
 
   Requires **Node.js >= v11**!
   
   The `setup/setup.ts`-file creates the necessary environment for the example. 
   A blank docker-deployment consisting of two authorities as described in 
   the [deploy-example](../deploy/README.md) is expected.
   
   Execution:
   ````
   npx ts-node setup/setup.ts
   ````
 * Publishers
 
   There are two publishers, one for each authority. 
   The publish processes update an existing book fact in their respective LDPs (increase price) or create it otherwise.
   The processes terminate after execution. 
   The proceses expect an environment provided by the setup.
   
   Execution:
   ````
   npx ts-node publisher/auth1/publishBooks.ts
   npx ts-node publisher/auth2/publishBooks.ts
   ````
 * Book Bundler
    
   This example process shows the usage of subscriptions.
 
   The book bundler offers a discounted prices for both books from the two different authorities together.
   It subscribes to the Resources created by the publishers (the resources are expected to exist on startup).
   Each time the book facts are changed, the bundle price is updated. The book bundler stays active as long as 
   a connection to the message broker persists.
   
   Execution:
   ````
   npx ts-node bookbundler/bookBundler.ts
   ````
   
 * Average Over Time
      
   This example shows the usage of timemaps.
   
   The avgOverTime process subscribes to the book fact of authority 1 and calculates the average price
   over all previous revisions every time the book fact is changed and stores it in a separate fact. 
   The process stays active as long as a connection to the message broker persists.
   The fact is expected to exist on startup.
     
   Execution:
   ````
   npx ts-node averagePriceOverTime/avgOverTime.ts
   ````