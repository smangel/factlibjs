import {ApplicationConfig, LdpMementoClient, logger, ProcessLifecycleManager} from "../../index";
import {Stream} from "stream";

const config: ApplicationConfig = require('./config.json');
const pHelper = new ProcessLifecycleManager(config);
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const fs = require("fs");
const resourceID = idFactory.createResourceID("http://localhost:8081", "facts/picture");

async function deleteBinaryFact() {
    try {
        await service.deleteResource(resourceID);
    } catch (e) {
        logger.info(e);
    }
}

async function getBinaryFact() {
    try {
        const response = await service.getLastBinaryFactRevision(resourceID, "image/png");
        if (response.binaryContent instanceof Stream) {
            response.binaryContent.pipe(fs.createWriteStream("../examples/binaryExample/output/logo.png"));
        } else {
            console.log(response.binaryContent);
        }

    } catch (e) {
        logger.info(e);
    }
}

async function createBinaryFact() {
    // If the processLifecycleManager is not used to gnerate the Activity, it must be done manually.
    const activityCandidate = pHelper.activity.createCandidate();
    // Activities should be associated with their respective process.
    activityCandidate.associateWithProcess(pHelper.process);
    // The activity should contain not contain FactDependencies that were used in previous executions.
    activityCandidate.clearProvUse();
    // The activity must be created before the fact.
    const activity = await service.createActivityRevision(activityCandidate);
    /*
    To create a binary Fact, the binary data is uploaded to the LDP.
    The LDP automatically creates RDF metadata.
    */
    const candidate = service.getFactDagResourceFactory().createEmptyBinaryFactCandidate(resourceID, fs.createReadStream("../examples/binaryExample/logo.png"), "image/png");
    // The correct PROV relations should be added.
    candidate.attributeToAuthority(idFactory.createAuthorityResourceID(pHelper.authority.resourceID.resourceURI.toString()));
    const fact = await service.createBinaryFactResource(candidate);
}

/**
 * Try to upload an image to the LDP.
 * If the resource already exists, download the image and delete it from the LDP.
 */
async function doThings(): Promise<void> {
    try {
        await pHelper.start();
    } catch (e) {
        logger.error(e);
    }
    try {
         await createBinaryFact();
         logger.info("Uploaded Image!");
     } catch (e) {
         if (e.response.status === 409) {
             await getBinaryFact();
             logger.info("Downloaded Image!");
             await deleteBinaryFact();
             logger.info("Deleted Image!");
         }
     }
}

doThings();
